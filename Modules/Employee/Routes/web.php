<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::prefix('/employees')->middleware('auth', 'verified')->group(function () {
    Route::get('/', [\Modules\Employee\Http\Controllers\EmployeeController::class, 'index'])->name('employees.index');
    Route::get('/getEmployees', [\Modules\Employee\Http\Controllers\EmployeeController::class, 'getEmployee'])->name('employees.getEmployees');
    Route::get('/create', [\Modules\Employee\Http\Controllers\EmployeeController::class, 'create'])->name('employees.create');
    Route::post('/store', [\Modules\Employee\Http\Controllers\EmployeeController::class, 'store'])->name('employees.store');
    Route::get('/edit/{id}/', [\Modules\Employee\Http\Controllers\EmployeeController::class, 'edit'])->name('employees.edit');
    Route::put('/update/{id}', [\Modules\Employee\Http\Controllers\EmployeeController::class, 'update'])->name('employees.update');
    Route::delete('/delete/{id}', [\Modules\Employee\Http\Controllers\EmployeeController::class, 'destroy'])->name('employees.destroy');
    Route::get('/count', [\Modules\Employee\Http\Controllers\EmployeeController::class, 'count'])->name('employees.conut');
});
