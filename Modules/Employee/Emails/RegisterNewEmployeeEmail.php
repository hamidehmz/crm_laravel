<?php

namespace Modules\Employee\Emails;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Modules\Employee\Entities\Employee;

class RegisterNewEmployeeEmail extends Mailable
{
    use Queueable, SerializesModels;

    protected Employee $employee;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Employee $employee)
    {
        $this->employee = $employee;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('employee::employees.RegisterEmployeeMail', [
            'first_name' => $this->employee->first_name,
            'last_name' => $this->employee->last_name,
            'email' => $this->employee->email,
            'phone' => $this->employee->phone,
            'company_name' => $this->employee->company_name,
        ]);
    }
}
