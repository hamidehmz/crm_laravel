<?php

namespace Modules\Employee\Database\Seeders;

use Faker\Factory as Faker;
use Illuminate\Database\Seeder;
use Modules\Company\Entities\Company;
use Modules\Employee\Entities\Employee;


class EmployeeDatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create();

        for ($i=1; $i < 10 ; $i++) {
            $employee = new Employee();
            $employee->first_name = $faker->name;
            $employee->last_name = $faker->lastName;
            $employee->email = $faker->email;
            $employee->phone = $faker->phoneNumber;
            $employee->company_id = $i;
            $company = Company::where('id',$i)->pluck('name');
            $employee->company_name = $company;
            $employee->address = $faker->address();
            $employee->save();
        }
    }
}
