
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Employees list</title>
    <link rel="icon"  href="{{ URL::asset('img/icon.png') }}">

    <!-- Datatables CSS  -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <link rel="stylesheet" href="/bootstrap-5.0.2-dist/css/bootstrap.min.css" >
    <link rel="stylesheet" href="/datatables.min.css">

    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">

    @vite(['resources/js/app.js'])

</head>
<body>
<div class="mb-3">
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Laravel') }}
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav me-auto">
                    </ul>
                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ms-auto">
                        <!-- Authentication Links -->
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item">
                                <a class="nav-link" href="{{route('companies.index')}}">Companies List</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link " href="{{route('employees.index')}}">Employees List</a>
                            </li>

                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-end" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">

            <div class="container mt-2">
                <div class="pull-left">
                    <h2>Employees List</h2>
                </div>
                    <div class="row">
                        <div class="col-lg-12 margin-tb">
                        @if(auth()->user()->hasRole(config('constants.ADMIN_NAME')))
                            <div class="pull-right mb-2">
                                <a class="btn btn-success" href="{{ route('employees.create') }}"> Create New Employee</a>
                            </div>
                            @endif
                        </div>
                    </div>
                @if(Session::get('success'))
                    @include('flash::message')
                @endif
                <div class="card-body">
                    <table class="table table-bordered" id="datatable-crud">
                        <thead>
                        <tr>
                            <th>First Name</th>
                            <th>Last Name</th>
                            <th>Email</th>
                            <th>Phone</th>
                            <th>Company Name</th>
                            @if(auth()->user()->hasRole(config('constants.ADMIN_NAME')))
                                <th>Action</th>
                            @endif
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </main>
    </div>
</div>
<script src="/jquery.min.js"></script>
<!-- DataTables -->
<script src="/datatables.min.js"></script>
<!-- Bootstrap JavaScript -->
<script src="/bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>
<!-- App scripts -->
@stack('scripts')

</body>
<script type="text/javascript">
    $(document).ready( function () {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        $('#datatable-crud').DataTable({
            processing: true,
            serverSide: true,
            ajax: "{{route('employees.index')}}",
            columns: [
                { data: 'first_name', name: 'first_name' },
                { data: 'last_name', name: 'last_name' },
                { data: 'email', name: 'email' },
                { data: 'phone', name: 'phone' },
                { data: 'company_name', name: 'company_name' },
                @if(auth()->user()->hasRole(config('constants.ADMIN_NAME')))
                {data: 'action', name: 'action', orderable: false},
                @endif
            ],
            order: [[0, 'desc']]
        });
    });
</script>
</html>
