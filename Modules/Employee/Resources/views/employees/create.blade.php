@extends('employee::layouts.app')

@section('content')
    @if($errors->any())
        <div class="alert alert-danger" >
            <ul>
                @foreach($errors->all() as $error)
                    <li>
                        {{$error}}
                    </li>
                @endforeach
            </ul>
        </div>
    @endif
    <div class="container mt-2">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="pull-left">
                    <h2>Create New Employee</h2>
                </div>
                <div class="pull-right">
                    <a class="btn btn-primary" href="{{ route('employees.index') }}" >Back</a>
                </div>
            </div>
        </div>
        <div class="container">
            <form action="{{route('employees.store')}}" method="post">
                @csrf
                <div class="form-row">
                    <div class="col-md-2"></div>

                    <div class="form-group col-md-4">
                        <label for="first_name">first_name</label>
                        <input type="text" name="first_name" class="form-control" autofocus>
                    </div>


                    <div class="form-group col-md-4">
                        <label for="last_name">last_name</label>
                        <input type="text" name="last_name" class="form-control">
                    </div>

                    <div class="col-md-2"></div>

                </div>

                <div class="form-row">
                </div>

                <div class="form-row">
                    <div class="col-md-2"></div>

                    <div class="form-group col-md-4">
                        <h6>Select Company</h6>
                        <select class="form-select" aria-label="Default select example" name="company">

                            @foreach($companies as $key => $company)

                                <option value="{{$company->id}}">{{$company->name}} </option>

                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4">
                        <label for="email">email</label>
                        <input type="email" name="email" class="form-control">
                    </div>
                </div>

                <div class="form-row">
                    <div class="col-md-2"></div>

                    <div class="form-group col-md-4">
                        <label for="phone">phone</label>
                        <input type="phone" name="phone" class="form-control">
                    </div>

                    <div class="form-group col-md-4">
                        <label for="address">address</label>
                        <input type="text" name="address" class="form-control">
                    </div>

                    <x-submit-button>
                        @slot('class')
                            btn btn-success ml-3
                        @endslot
                        Submit
                    </x-submit-button>

                </div>
            </form>
        </div>

@endsection
