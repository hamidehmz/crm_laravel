<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <title>A New Employee Has Been Created</title>
</head>
<body>

<h4>Hello Admin!</h4>

<p>A employee has been created with the following information .</p>

<p>First Name : {{$first_name}}</p>
<p>Last Name : {{$last_name}}</p>
<p>Email : {{$email}}</p>
<p>Phone : {{$phone}}</p>
<p>Company_name : {{$company_name}}</p>

</body>
</html>
