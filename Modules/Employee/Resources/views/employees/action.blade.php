<div class="row">
    <div class="col-sm-3  text-white">
        {{$id=\Vinkla\Hashids\Facades\Hashids::encode($id)}}
        <a href="{{ route('employees.edit',$id) }}" class="edit btn btn-primary edit">
            Edit
        </a>
    </div>
    <div class="col-sm-9  text-white">
        <form action="{{ route('employees.destroy',$id) }}"  method="post" >
            @csrf
            @method('delete')
            <button class="btn btn-danger" >Delete</button>
        </form>

    </div>
</div>



