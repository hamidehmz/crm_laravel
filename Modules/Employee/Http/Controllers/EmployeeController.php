<?php

namespace Modules\Employee\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Support\Facades\DB;
use Modules\Company\Entities\Company;
use Modules\Employee\Entities\Employee;
use Modules\Employee\Jobs\SendRegisterEmployeeEmail;
use Vinkla\Hashids\Facades\Hashids;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        if (request()->ajax()) {
            return datatables()->of(Employee::select('*'))
                ->addColumn('action', 'employee::employees.action')
                ->rawColumns(['action'])
                ->addIndexColumn()
                ->make(true);
        }

        return view('employee::employees.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Renderable
     */
    public function create()
    {
        return view('employee::employees.create', [
            'companies' => Company::all(),
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  Request  $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:employees'],
            'phone' => ['required', 'max:255'],
            'address' => ['required', 'max:255'],

        ]);

        $employee = new Employee();
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->address = $request->address;
        $employee->company_id = $request->input('company');

        $employee->company_name = DB::table('companies')
            ->where('id', $employee->company_id)->value('name');

        $employee->save();
        dispatch(new SendRegisterEmployeeEmail($employee));

        return redirect()->route('employees.index')
         ->with('success', flash('Employee has been created successfully')->success());
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     * @return Renderable
     */
    public function show($id)
    {
        return view('employee::show');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Renderable
     */
    public function edit($id)
    {
        $id = Hashids::decode($id);
        $employee = Employee::where('id', $id)->first();

        return view('employee::employees.edit', [
            'employee' => $employee,
            'companies' => Company::all(),

        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'first_name' => ['required', 'string', 'max:255'],
            'last_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255'],
            'phone' => ['required', 'max:255'],

        ]);

        $employee = Employee::find($id);
        $employee->first_name = $request->first_name;
        $employee->last_name = $request->last_name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->company_id = $request->input('company');
        $employee->company_name = DB::table('companies')
            ->where('id', $employee->company_id)->value('name');

        $employee->save();

        return redirect()->route('employees.index')
        ->with('success', flash('Employee has been updated successfully')->success());
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Request $request, $id)
    {
        $id = Hashids::decode($id);

        $employee = Employee::where('id', $id)->first();

        return redirect()->route('employees.index')
            ->with('success', flash('Employee has been deleted successfully')->success());
    }

    public function count()
    {
        $employee = DB::table('employees')
            ->select(DB::raw('count(*) as employee_count, company_id'))
            ->where('company_id', '<>', 0)
            ->groupBy('company_id')
            ->get();
    }
}
