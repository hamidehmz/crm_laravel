
<h2>Employee Name: </h2>

@foreach($employees as $employee)
    <p>{{ $employee->first_name }} {{ $employee->last_name }}</p>

    <h3>Tasks</h3>

    <ul>
        @foreach($employee->tasks as $task)
            <li>{{ $task->title }}</li>
        @endforeach
    </ul>

@endforeach



