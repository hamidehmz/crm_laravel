<?php

namespace Modules\Task\Entities;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Modules\Employee\Entities\Employee;

class Task extends Model
{
    use HasFactory;

    protected $fillable = [];

    public function employees()
    {
        return $this->belongsToMany(Employee::class);
    }
}
