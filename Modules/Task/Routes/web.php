<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('task/create', [\Modules\Task\Http\Controllers\TaskController::class, 'create'])->name('task.create');

Route::get('task/show', [\Modules\Task\Http\Controllers\TaskController::class, 'show'])->name('task.show');
