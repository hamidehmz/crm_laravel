<?php

namespace Modules\User\Tests\Unit;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modules\User\Entities\User;
use Tests\TestCase;

class UserCreateTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * A basic unit test example.
     *
     * @return void
     */
    public function testuser()
    {
        $new_user = User::create([
            'name' => 'Mekaeil',
            'email' => 'm@gmail.com',
            'password' => '123123123',
        ]);

        $this->assertEquals('Mekaeil', $new_user->name);
    }
}
