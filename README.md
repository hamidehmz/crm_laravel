# Mini CRM
Mini-CRM is a system for managing companies, employees and employee's tasks that is designed to be modular.

## Features
- Auth and verification User
- send email to admin after create employee
- send email happy birthdate to user 
- Create new companies and employees only by admin

## Packages

-  Use Laravel/breeze for Auth and verification             
-  Use Datatable.net library for showing companies and employees list 
-  use nwidart package for create module(user,company,employee and task)

- Use Morilog package to show solar calendar
- Use Hashids package

-   Use Laravel pint
- Add laravel/horizon (provides a beautiful dashboard and code-driven configuration for your Laravel powered Redis queues.)
-  Add "arcandev/laravel" log viewer
- Use "spatie/image" package for create 3 size of company image
-   Use "spatie/laravel-image-optimizer": "^1.7" for optimize image
- Use "spatie/laravel-permission" to assign admin role

# Installation
For server requirements, and a general how-to referee to Laravel's original documentation at: https://laravel.com/docs/9.x/installation



## Basic How to
- Insert project into empty folder/ 
```bash
git clone https://gitlab.com/hamidehmz/crm_laravel
```
- Insert submodule into empty folder/ 
```bash
git clone https://gitlab.com/hamidehmz/company
```
- Create an empty database
- Copy the .env.example to .env
- Configure your database config in either config/database.php or in your .env file
- Run the following commands
```php
composer install
php artisan migrate --seed
php artisan key:generate
npm install
npm run dev
```

```bash
git submodule add https://gitlab.com/hamidehmz/company  Module/Company

```
- login in with these credentials
Mail: admin@admin.tech Password: 12345678

- Done
The above is required that you have PHP, MySQL,Redis etc. installed locally on your computer 

Next Steps
You can now easily insert dummy data into the CRM, or start using it freely Insertion of dummy data
