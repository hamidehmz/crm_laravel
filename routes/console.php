<?php

use Illuminate\Foundation\Inspiring;
use Illuminate\Support\Facades\Artisan;
use Modules\User\Entities\User;

/*
|--------------------------------------------------------------------------
| Console Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of your Closure based console
| commands. Each Closure is bound to a command instance allowing a
| simple approach to interacting with each command's IO methods.
|
*/

Artisan::command('inspire', function () {
    $this->comment(Inspiring::quote());
})->purpose('Display an inspiring quote');

Artisan::command('user:exists {value} {column=email}', function ($column, $value) {
    $emailExists = User::where($column, $value)->exists();
    if ($emailExists) {
        $this->info('User Already exists');
    } else {
        $this->error('User does not exist');
    }
});
