<?php

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Modules\Company\Entities\Company;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class CompanyType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Company',
        'description' => 'Collection of companies',
        'model' => Company::class,
    ];

    public function fields(): array
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int()),
                'description' => 'ID of company',
            ],
            'name' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Title of the company',
            ],
            'email' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'Email of the company',
            ],
            'website' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'website of the company',
            ],
            'action' => [
                'type' => GraphQL::type('Action'),
                'description' => 'list of the Action',
                'resolve' => function ($root, $args) {
                    return $root;
                },
            ],
        ];
    }
}
