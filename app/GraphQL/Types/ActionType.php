<?php

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Type as GraphQLType;

class ActionType extends GraphQLType
{
    protected $attributes = [
        'name' => 'Action',
        'description' => 'Collection of action',
    ];

    public function fields(): array
    {
        return [
            'edit' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'edit action link',
                'resolve' => function ($root, $args) {
                    return route('companies.edit', ['company' => $root->id]);
                },
            ],
            'show' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'show action link',
                'resolve' => function ($root, $args) {
                    return route('companies.show', ['company' => $root->id]);
                },
            ],
            'delete' => [
                'type' => Type::nonNull(Type::string()),
                'description' => 'delete action link',
                'resolve' => function ($root, $args) {
                    return route('companies.destroy', ['company' => $root->id]);
                },
            ],
        ];
    }
}
