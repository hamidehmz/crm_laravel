<?php

namespace App\GraphQL\Types;

use GraphQL\Type\Definition\Type;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Type as GraphQLType;

class DatatableType extends GraphQLType
{
    protected $attributes = [
        'name' => 'DatatableType',
        'description' => 'Collection of companies',
    ];

    public function fields(): array
    {
        return [
            'draw' => [
                'type' => Type::int(),
                'description' => 'draw of the datatable',
            ],
            'recordsTotal' => [
                'type' => Type::int(),
                'description' => 'records total of the datatable',
            ],
            'recordsFiltered' => [
                'type' => Type::int(),
                'description' => 'records filtered of the datatable',
            ],
            'data' => [
                'type' => Type::listOf(GraphQL::type('Company')),
                'description' => 'list of the Company',
                'is_relation' => false,
            ],
        ];
    }
}
