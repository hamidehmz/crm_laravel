<?php

namespace App\GraphQL\Queries;

use GraphQL\Type\Definition\Type;
use Illuminate\Pagination\Paginator;
use Modules\Company\Entities\Company;
use Rebing\GraphQL\Support\Facades\GraphQL;
use Rebing\GraphQL\Support\Query;

class CompaniesQuery extends Query
{
    protected $attributes = [
        'name' => 'Companies',
    ];

    public function type(): Type
    {
        return GraphQL::type('DatatableType');
    }

    public function args(): array
    {
        return [
            'sort_field' => [
                'name' => 'sort_field',
                'type' => Type::string(),
                'rules' => ['nullable'],
            ],
            'sort_type' => [
                'name' => 'sort_type',
                'type' => Type::string(),
                'rules' => ['nullable'],
            ],
            'search_key' => [
                'name' => 'search_key',
                'type' => Type::string(),
                'rules' => ['nullable'],
            ],
            'per_page' => [
                'name' => 'per_page',
                'type' => Type::int(),
                'defaultValue' => 15,
                'rules' => ['nullable'],
            ],
            'current_page' => [
                'name' => 'current_page',
                'type' => Type::int(),
                'rules' => ['nullable'],
            ],
        ];
    }

    public function resolve($root, $args)
    {
        Paginator::currentPageResolver(function () use ($args) {
            return $args['current_page'];
        });

        $companies = Company::paginate($args['per_page']);

        return [
            'draw' => 1,
            'recordsTotal' => $companies->total(),
            'recordsFiltered' => $companies->total(),
            'data' => $companies,

        ];
    }
}
